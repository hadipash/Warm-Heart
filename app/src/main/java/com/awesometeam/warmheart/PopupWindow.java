package com.awesometeam.warmheart;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;

import static android.R.attr.defaultValue;

public class PopupWindow extends Service {
    LinearLayout layout;
    private static WindowManager windowManager;
    Timer timer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (windowManager == null) {
            final Intent intentOut = new Intent(this, RoutActivity.class);
            intentOut.putExtra("SrcLat", intent.getDoubleExtra("SourceLat", defaultValue));
            intentOut.putExtra("SrcLong", intent.getDoubleExtra("SourceLong", defaultValue));
            intentOut.putExtra("DestLat", intent.getDoubleExtra("DestinationLat", defaultValue));
            intentOut.putExtra("DestLong", intent.getDoubleExtra("DestinationLong", defaultValue));
            intentOut.putExtra("PhoneNumber", intent.getStringExtra("PhoneNumber"));
            intentOut.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = (LinearLayout) inflater.inflate(R.layout.popup_window, null);
            Button noBtn = (Button) layout.findViewById(R.id.NoButton);
            Button yesBtn = (Button) layout.findViewById(R.id.YesButton);

            //get device ring state and vibration or RingBell
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask(){
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone ring = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                int mod = am.getRingerMode();

                @Override
                public void run() {
                    if (mod == AudioManager.RINGER_MODE_VIBRATE)
                        mVibrator.vibrate(500);
                    else if (mod == AudioManager.RINGER_MODE_NORMAL) {
                        ring.play();
                        mVibrator.vibrate(500);
                    }
                }
            }, 0, 1000);

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    onDestroy();
                }
            }, 120000);


            final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                    PixelFormat.TRANSLUCENT);

            params.gravity = Gravity.CENTER;
            windowManager.addView(layout, params);

            try {
                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onDestroy();
                        SettingActivity.IncreaseAcceptNum();
                        startActivity(intentOut);
                    }
                });

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onDestroy();
                    }
                });
            } catch (Exception e) {
                Log.e("PopupService", "Exception: " + e.getMessage());
            }
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        if (layout != null) windowManager.removeView(layout);
        windowManager = null;
    }
}
