package com.awesometeam.warmheart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ManualActivity_con3 extends AppCompatActivity {
    int image = Con3_DATA.image;
    String name[] = Con3_DATA.content;
    String subcontent[] = Con3_DATA.subcontent;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Con3_Adapter recAdapter;
    private List<Con3_Parent_Model> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_con3);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManualActivity_con3.this.finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView3);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        list = getList();
        recAdapter = new Con3_Adapter(this, list);
        recyclerView.setAdapter(recAdapter);
    }


    private List<Con3_Parent_Model> getList() {
        list = new ArrayList<>();
        for (int i = 0; i < name.length; i++) {
            List<Con3_Child_Model> child_models = new ArrayList<>();
            Con3_Child_Model child_model = new Con3_Child_Model(subcontent[i]);
            child_models.add(child_model);
            Con3_Parent_Model pm = new Con3_Parent_Model(name[i], child_models, image);
            list.add(pm);
        }
        return list;
    }
}
