package com.awesometeam.warmheart;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.microsoft.windowsazure.notifications.NotificationsHandler;

import static com.awesometeam.warmheart.MainActivity.lastLocation;

public class MyHandler extends NotificationsHandler {
    //public static final int NOTIFICATION_ID = 1;
    Context ctx;
    //private NotificationManager mNotificationManager;

    @Override
    public void onReceive(Context context, Bundle bundle) {
        if (!MainActivity.sentbyme && SettingActivity.GetPushStatus()) {
            ctx = context;
            double latitude = Double.parseDouble(bundle.getString("lat"));
            double longitude = Double.parseDouble(bundle.getString("long"));
            String phone = bundle.getString("phone");
            Location receivedLocation = new Location("Received Location");
            receivedLocation.setLatitude(latitude);
            receivedLocation.setLongitude(longitude);
            Double mylat = lastLocation.getLatitude();
            Double mylong = lastLocation.getLongitude();

            Log.e("debug", String.valueOf(latitude) + ", " + String.valueOf(longitude));
            Log.e("mylocation", String.valueOf(mylat) + ", " + String.valueOf(mylong));

            Double dist = lastLocation.distanceTo(receivedLocation) * 0.001;
            Log.e("dif_dist", String.valueOf(dist));

            if (dist <= 1) {
                sendNotification(latitude, longitude, mylat, mylong, phone);
            }
        } else {
            MainActivity.sentbyme = false;
        }
    }

    private void sendNotification(double destLat, double destLong, double srcLat, double srcLong, String phone) {
        /*Intent intent = new Intent(ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notification Hub Demo")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setSound(defaultSoundUri)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());*/

        Intent intent2 = new Intent(ctx, PopupWindow.class);
        intent2.putExtra("DestinationLat", destLat);
        intent2.putExtra("DestinationLong", destLong);
        intent2.putExtra("SourceLat", srcLat);
        intent2.putExtra("SourceLong", srcLong);
        intent2.putExtra("PhoneNumber", phone);
        ctx.startService(intent2);
    }
}
