package com.awesometeam.warmheart;

/**
 * Created by JIA on 2017-05-22.
 */

public class Con1_Rec_Data {
    private String num;
    private String content;
    private int thumbnail;

    public Con1_Rec_Data(String num, String content, int thumbnail) {
        this.content = content;
        this.thumbnail = thumbnail;
        this.num = num;
    }

    public String getNum() {
        return num;
    }
    public String getContent() {
        return content;
    }
    public int getThumbnail() {
        return thumbnail;
    }
}
