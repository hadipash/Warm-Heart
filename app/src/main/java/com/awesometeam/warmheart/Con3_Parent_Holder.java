package com.awesometeam.warmheart;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by JIA on 2017-06-22.
 */

public class Con3_Parent_Holder extends GroupViewHolder {
    private TextView parent_title;
    private ImageView parent_image;

    public Con3_Parent_Holder(View itemView) {
        super(itemView);
        parent_title = (TextView)itemView.findViewById(R.id.parenttitle);
        parent_image = (ImageView)itemView.findViewById(R.id.smile);
    }

    public void setParent_title(String contexnt){
        parent_title.setText(contexnt);
    }
}
