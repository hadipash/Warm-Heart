package com.awesometeam.warmheart;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JIA on 2017-05-22.
 */

public class Con1_Adapter extends RecyclerView.Adapter<Con1_Adapter.MyViewHolder> {
    private ArrayList<Con1_Rec_Data> dataSet;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView thumbnail;
        public TextView textNum;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textNum = (TextView)itemView.findViewById(R.id.card_num);
            this.textView = (TextView)itemView.findViewById(R.id.card_text);
            this.thumbnail = (ImageView)itemView.findViewById(R.id.card_header);
        }
    }

    public Con1_Adapter(ArrayList<Con1_Rec_Data> viewDatas) {
        this.dataSet = viewDatas;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("create", "ok");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menual_card, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(itemView);
        return myViewHolder;
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TextView textView = holder.textView;
        ImageView thumbnail = holder.thumbnail;
        TextView textNum = holder.textNum;

        Log.d("data","enter bind ok");
        textNum.setText(dataSet.get(position).getNum());
        textView.setText(dataSet.get(position).getContent());
        Log.d("data","enter getcontent ok");
        thumbnail.setImageResource(dataSet.get(position).getThumbnail());
        Log.d("data","getThumnail ok");
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
