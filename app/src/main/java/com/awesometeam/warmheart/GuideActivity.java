package com.awesometeam.warmheart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class GuideActivity extends AppCompatActivity {
    TextView text_aed, text_menual, text_help, text_mark, text_setting;
    ImageButton button_1, button_2, button_3, button_4, button_5;
    ImageView title_aed, title_menual, title_help, title_mark, title_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        text_aed = (TextView) findViewById(R.id.Text1_title);
        text_menual = (TextView) findViewById(R.id.Text2_title);
        text_help = (TextView) findViewById(R.id.Text3_title);
        text_mark = (TextView) findViewById(R.id.Text4_title);
        text_setting = (TextView) findViewById(R.id.Text5_title);

        title_aed = (ImageView) findViewById(R.id.guide_aed_title);
        title_menual = (ImageView) findViewById(R.id.guide_manual_title);
        title_help = (ImageView) findViewById(R.id.guide_help_title);
        title_mark = (ImageView) findViewById(R.id.guide_mark_title);
        title_setting = (ImageView) findViewById(R.id.guide_setting_title);

        text_aed.setVisibility(View.INVISIBLE);
        text_menual.setVisibility(View.INVISIBLE);
        text_help.setVisibility(View.INVISIBLE);
        text_mark.setVisibility(View.INVISIBLE);
        text_setting.setVisibility(View.INVISIBLE);

        title_aed.setVisibility(View.INVISIBLE);
        title_menual.setVisibility(View.INVISIBLE);
        title_help.setVisibility(View.INVISIBLE);
        title_mark.setVisibility(View.INVISIBLE);
        title_setting.setVisibility(View.INVISIBLE);

        button_1 = (ImageButton) findViewById(R.id.number1);
        button_2 = (ImageButton) findViewById(R.id.number2);
        button_3 = (ImageButton) findViewById(R.id.number3);
        button_4 = (ImageButton) findViewById(R.id.number4);
        button_5 = (ImageButton) findViewById(R.id.number5);

        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_aed.setVisibility(View.VISIBLE);
                title_aed.setVisibility(View.VISIBLE);
                title_menual.setVisibility(View.INVISIBLE);
                title_help.setVisibility(View.INVISIBLE);
                title_mark.setVisibility(View.INVISIBLE);
                title_setting.setVisibility(View.INVISIBLE);
                text_menual.setVisibility(View.INVISIBLE);
                text_help.setVisibility(View.INVISIBLE);
                text_mark.setVisibility(View.INVISIBLE);
                text_setting.setVisibility(View.INVISIBLE);
            }
        });

        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_menual.setVisibility(View.VISIBLE);
                title_menual.setVisibility(View.VISIBLE);
                text_aed.setVisibility(View.INVISIBLE);
                text_aed.setVisibility(View.INVISIBLE);
                text_help.setVisibility(View.INVISIBLE);
                text_mark.setVisibility(View.INVISIBLE);
                text_setting.setVisibility(View.INVISIBLE);
                title_help.setVisibility(View.INVISIBLE);
                title_mark.setVisibility(View.INVISIBLE);
                title_setting.setVisibility(View.INVISIBLE);
            }
        });

        button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_help.setVisibility(View.VISIBLE);
                title_help.setVisibility(View.VISIBLE);
                text_aed.setVisibility(View.INVISIBLE);
                text_menual.setVisibility(View.INVISIBLE);
                text_mark.setVisibility(View.INVISIBLE);
                text_setting.setVisibility(View.INVISIBLE);
                title_aed.setVisibility(View.INVISIBLE);
                title_menual.setVisibility(View.INVISIBLE);
                title_mark.setVisibility(View.INVISIBLE);
                title_setting.setVisibility(View.INVISIBLE);
            }
        });

        button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_mark.setVisibility(View.VISIBLE);
                title_mark.setVisibility(View.VISIBLE);
                text_aed.setVisibility(View.INVISIBLE);
                text_menual.setVisibility(View.INVISIBLE);
                text_help.setVisibility(View.INVISIBLE);
                text_setting.setVisibility(View.INVISIBLE);
                title_aed.setVisibility(View.INVISIBLE);
                title_menual.setVisibility(View.INVISIBLE);
                title_help.setVisibility(View.INVISIBLE);
                title_setting.setVisibility(View.INVISIBLE);
            }
        });

        button_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_setting.setVisibility(View.VISIBLE);
                title_setting.setVisibility(View.VISIBLE);
                text_aed.setVisibility(View.INVISIBLE);
                text_menual.setVisibility(View.INVISIBLE);
                text_help.setVisibility(View.INVISIBLE);
                text_mark.setVisibility(View.INVISIBLE);
                title_aed.setVisibility(View.INVISIBLE);
                title_menual.setVisibility(View.INVISIBLE);
                title_help.setVisibility(View.INVISIBLE);
                title_mark.setVisibility(View.INVISIBLE);
            }
        });
    }
}