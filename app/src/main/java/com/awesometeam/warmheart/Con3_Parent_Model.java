package com.awesometeam.warmheart;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by JIA on 2017-06-22.
 */

public class Con3_Parent_Model  extends ExpandableGroup {
    private int smile;
    public Con3_Parent_Model(String title, List items, int smile) {
        super(title, items);
        this.smile = smile;
    }

    public int getSmile() {
        return smile;
    }
}
