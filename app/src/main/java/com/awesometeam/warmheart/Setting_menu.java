package com.awesometeam.warmheart;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.test.suitebuilder.TestMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Setting_menu extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView number_title;
    private TextView number_content;
    private ImageView number1;
    private ImageView number2;
    private ImageView number3;
    private ImageView number4;
    private ImageView number5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_menu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Setting_menu.this.finish();
            }
        });

        number1 = (ImageView) findViewById(R.id.number1);
        number2 = (ImageView) findViewById(R.id.number2);
        number3 = (ImageView) findViewById(R.id.number3);
        number4 = (ImageView) findViewById(R.id.number4);
        number5 = (ImageView) findViewById(R.id.number5);
        number_title = (TextView) findViewById(R.id.number_title);
        number_content = (TextView) findViewById(R.id.number_content);

        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number_title.setText("1. AED찾기");
                number_content.setText("AED찾기를 누르시면 주변 AED의 위치가 보여집니다.");
            }
        });

        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number_title.setText("2. 응급처치매뉴얼");
                number_content.setText("AED사용방법과 심폐소생술방법이 나와있습니다.\n 심폐소생술에 관한 자주묻는 질문에 대한 답변이 있습니다.");
            }
        });

        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number_title.setText("3. Help기능");
                number_content.setText("Helper에게 도움을 청하고 119에 신고메세지가 갑니다.\n 악용시 법척처벌 가능하니 주의하십시오.");
            }
        });

        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number_title.setText("4. 내 위치 확인하기");
                number_content.setText("정확하지 않은 위치가 뜨고 있다면 이 버튼을 눌러 위치를 재설정해주세요.");
            }
        });


        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number_title.setText("5. Warm Heart 환경설정");
                number_content.setText("Helper가 알람을 받을지 안받을지 설정하기, 이 때까지 Help를 accept한 횟수가 나와있습니다.");
            }
        });
    }
}
