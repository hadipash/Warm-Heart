package com.awesometeam.warmheart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class ManualActivity_con2 extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private Con1_Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Con1_Rec_Data> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_con2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManualActivity_con2.this.finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Log.d("data","set ok");

        data = new ArrayList<Con1_Rec_Data>();
        Log.d("data","arraylist ok");
        for(int i = 0; i < Con2_DATA.contentArray.length; i++) {
            data.add(new Con1_Rec_Data(
                    Con2_DATA.num[i],
                    Con2_DATA.contentArray[i],
                    Con2_DATA.cprs[i]
            ));
            Log.d("data","data add ok");
        }

        adapter = new Con1_Adapter(data);
        Log.d("data","create adapter ok");
        recyclerView.setAdapter(adapter);
    }
}
