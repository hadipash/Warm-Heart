package com.awesometeam.warmheart;

/**
 * Created by JIA on 2017-05-28.
 */

public class Con3_Child_Model {
    private String content;

    public Con3_Child_Model() {
    }

    public String getContent() {
        return  content;
    }

    public Con3_Child_Model(String content){
        this.content = content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
