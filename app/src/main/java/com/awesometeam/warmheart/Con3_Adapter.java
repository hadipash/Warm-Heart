package com.awesometeam.warmheart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by JIA on 2017-05-28.
 */

public class Con3_Adapter extends ExpandableRecyclerViewAdapter<Con3_Parent_Holder, Con3_Child_Holder> {
    private Context context;

    public Con3_Adapter(Context context, List<? extends ExpandableGroup> groups) {
        super(groups);
        this.context = context;
    }

    @Override
    public Con3_Parent_Holder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parent_card, parent, false);
        return new Con3_Parent_Holder(view);
    }

    @Override
    public Con3_Child_Holder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_card, parent, false);
        return new Con3_Child_Holder(view);
    }

    @Override
    public void onBindChildViewHolder(Con3_Child_Holder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Con3_Child_Model child_model = (Con3_Child_Model) group.getItems().get(childIndex);
        holder.setSubTextView(child_model.getContent());
    }

    @Override
    public void onBindGroupViewHolder(Con3_Parent_Holder holder, int flatPosition, ExpandableGroup group) {
        holder.setParent_title(group.getTitle());
    }
}
