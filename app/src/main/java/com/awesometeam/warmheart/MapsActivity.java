package com.awesometeam.warmheart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Iterator;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    public static boolean aedlocationmarker = true;
    private ArrayList<Double> myLocation;
    private ArrayList<Double> latitude, longitude;
    private ArrayList<String> org, managerTel, address1, address2;
    private GoogleMap mMap;
    private Toolbar toolbar;
    private RelativeLayout infoWindow;
    private TextView addrText1, addrText2, orgText, telText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent intent = getIntent();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        infoWindow = (RelativeLayout) findViewById(R.id.infoWindow);
        addrText1 = (TextView) findViewById(R.id.address1);
        addrText2 = (TextView) findViewById(R.id.address2);
        orgText = (TextView) findViewById(R.id.org);
        telText = (TextView) findViewById(R.id.managerTel);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapsActivity.this.finish();
            }
        });

        myLocation = (ArrayList<Double>) intent.getSerializableExtra("My Location");
        latitude = (ArrayList<Double>) intent.getSerializableExtra("latitude");
        longitude = (ArrayList<Double>) intent.getSerializableExtra("longitude");
        org = intent.getStringArrayListExtra("org");
        managerTel = intent.getStringArrayListExtra("managerTel");
        address1 = intent.getStringArrayListExtra("address1");
        address2 = intent.getStringArrayListExtra("address2");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Iterator<Double> myLoc = myLocation.iterator();

        Double myLat = myLoc.next();
        Double myLon = myLoc.next();

        mMap.addMarker(new MarkerOptions().position(new LatLng(myLat, myLon))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))).setTag(-1);

        try {
            // Add a marker in Sydney and move the camera
            Iterator<Double> iLat = latitude.iterator();
            Iterator<Double> iLon = longitude.iterator();

            ArrayList<Marker> markers = new ArrayList<>();

            Log.e("debug", "My location: " + myLat + ", " + myLon);

            for(int i = 0 ; iLat.hasNext() && iLon.hasNext() ; i++ ) {
                markers.add(createMarker(iLat.next(), iLon.next(), i));
            }
 /*
            while (iLat.hasNext() && iLon.hasNext() ) {
                mMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(iLat.next(), iLon.next()))
                );
            }
*/
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLat, myLon), 15));
        mMap.setOnMarkerClickListener(this);
    }

    protected Marker createMarker(Double lat, Double lon, int id) {
        Marker marker = mMap.addMarker( new MarkerOptions()
                .position(new LatLng(lat, lon))
        );
        marker.setTag(id);

        return marker;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int index = (int) marker.getTag();
        if (index != -1) {
            Log.e("debug", "marker number " + String.valueOf(index));
            aedlocationmarker = true;

            try {
                infoWindow.setVisibility(View.VISIBLE);
                orgText.setVisibility(View.VISIBLE);
                addrText1.setVisibility(View.VISIBLE);
                addrText2.setVisibility(View.VISIBLE);
                telText.setVisibility(View.VISIBLE);
                addrText1.setText(address1.get(index));
                orgText.setText(org.get(index));
                telText.setText(managerTel.get(index));
                addrText2.setText(address2.get(index));
                aedlocationmarker = false;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        else{
            infoWindow.setVisibility(View.INVISIBLE);
            orgText.setVisibility(View.INVISIBLE);
            addrText1.setVisibility(View.INVISIBLE);
            addrText2.setVisibility(View.INVISIBLE);
            telText.setVisibility(View.INVISIBLE);
        }
        return false;
    }
}