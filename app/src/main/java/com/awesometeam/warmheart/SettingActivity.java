package com.awesometeam.warmheart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class SettingActivity extends AppCompatActivity {
    private Switch sSwitch;
    private static Boolean push = null;
    private static Integer acceptNum = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (push == null) {
            ReadStorage();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingActivity.this.finish();
            }
        });

        TextView btn_setting_menu = (TextView) findViewById(R.id.setting_menu);
        btn_setting_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, Setting_menu.class));
            }
        });

        sSwitch = (Switch) findViewById(R.id.switch2);
        sSwitch.setChecked(push);
        sSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                push = sSwitch.isChecked();
                MainActivity.sp.edit().putBoolean("push", push).apply();
            }
        });

        TextView acceptText = (TextView) findViewById(R.id.textView2);
        acceptText.setText(acceptNum.toString());
    }

    private static void ReadStorage(){
        push = MainActivity.sp.getBoolean("push", true);
        acceptNum = MainActivity.sp.getInt("accept", 0);
    }

    public static boolean GetPushStatus() {
        if(push == null)
            ReadStorage();

        return push;
    }
    public static void IncreaseAcceptNum() {
        if(acceptNum == null)
            ReadStorage();

        MainActivity.sp.edit().putInt("accept", ++acceptNum).apply();
    }
}