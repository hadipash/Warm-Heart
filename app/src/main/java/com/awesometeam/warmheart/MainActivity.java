package com.awesometeam.warmheart;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    // Cloud messaging
    //public MainActivity mainActivity;
    public static Boolean sentbyme = false;
    private String HubEndpoint = null;
    private String HubSasKeyName = null;
    private String HubSasKeyValue = null;
    // Database connection
    private static String hostName = "warmheart.database.windows.net";
    private static String dbName = "AED Info";
    private static String user = "AppUser";
    private static String password = "4UdWA4Y8at";
    private static String url = String.format("jdbc:jtds:sqlserver://%s:1433;databasename=%s;user=%s;password=%s;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);
    private Connection connection = null;
    // Location
    int REQUEST_CHECK_SETTINGS = 1000;
    private LocationRequest locationRequest;
    private boolean locationEnable = false;
    private LinearLayout btn_manual;
    private ImageView btn_setting;
    private LinearLayout btn_aed;
    private ImageView btn_location;
    private Button btn_help;
    private ArrayList<Double> latitude;
    private ArrayList<Double> longitude;
    private ArrayList<String> serialNum;
    private ArrayList<String> org;
    private ArrayList<String> managerTel;
    private ArrayList<String> address1, address2;
    public static Location lastLocation;
    private static GoogleApiClient googleApiClient;
    // Messages
    private SmsManager smsManager;
    private String number;
    private String json = null;
    // Local storage
    public static SharedPreferences sp;

    public void setJson() {
        json = "{\"priority\" : \"high\",\"data\" : {\"lat\" : " +
                lastLocation.getLatitude() + ", \"long\" : " + lastLocation.getLongitude() + ", \"phone\" : \"" + number + "\"}}";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Cloud messaging
        //mainActivity = this;
        NotificationsManager.handleNotifications(this, NotificationSettings.SenderId, MyHandler.class);
        registerWithNotificationHubs();
        sp = getSharedPreferences("Storage", MODE_PRIVATE);

        btn_manual = (LinearLayout) findViewById(R.id.btn_manual);
        btn_setting = (ImageView) findViewById(R.id.btn_setting);
        btn_aed = (LinearLayout) findViewById(R.id.btn_aed);
        btn_location = (ImageView) findViewById(R.id.btn_location);
        btn_help = (Button) findViewById(R.id.btn_help);

        // Allow app to pop up windows
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MainActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 0);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
            }
        }

        requestPermission(PERMISSION_REQUEST_CODE_LOCATION, getApplicationContext(), MainActivity.this);
        if (locationEnable) {
            onStart();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(MainActivity.this)
                .addOnConnectionFailedListener(MainActivity.this)
                .build();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        checkForLocationReuqestSetting(locationRequest);
        googleApiClient.connect();

        new ConnectToDB().execute(url);

        btn_manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ManualActivity.class);
                startActivity(intent);
            }
        });

        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

        btn_aed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collectLocations();
            }
        });

        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Log.e("debug", "button Clicked");
                    checkForLocationReuqestSetting(locationRequest);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, MainActivity.this);
                    ((TextView) (findViewById(R.id.myLocation))).setText(getAddress(LocationServices.FusedLocationApi.getLastLocation(googleApiClient)));
                } catch (SecurityException ex) {
                    ex.printStackTrace();
                }
            }
        });

        /*
         * Send Notification button click handler. This method parses the
         * DefaultFullSharedAccess connection string and generates a SaS token. The
         * token is added to the Authorization header on the POST request to the
         * notification hub. The text in the editTextNotificationMessage control
         * is added as the JSON body for the request to add a GCM message to the hub.
         */
        btn_help.setOnClickListener(new View.OnClickListener() {
            TelephonyManager tm;

            @Override
            public void onClick(View v) {
                //get phone address ex)+8223658030
                tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                number = tm.getLine1Number();
                final String imei = tm.getDeviceId();
                setJson();
                Log.e("debug", "help clicked" + json);

                try {
                    final PopupWindow pw;

                    LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater.inflate(R.layout.help_popup, null);

                    // create a 300px width and 470px height PopupWindow
                    pw = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
                    // display the popup in the center
                    pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

                    Button cancelButton = (Button) layout.findViewById(R.id.NoButton);
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pw.dismiss();
                        }
                    });
                    Button okButton = (Button) layout.findViewById(R.id.YesButton);
                    okButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pw.dismiss();
                            sentbyme = true;

                            // Send message to 119
                            PendingIntent resultCode = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent("SMS_Sent"), 0);
                            smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage("01057513392", null, "심정지 환자 발생", resultCode, null);

                            // When the SMS has been sent
                            registerReceiver(new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context arg0, Intent arg1) {
                                    if (getResultCode() == Activity.RESULT_OK) {
                                        //ToastNotify("Emergency message has been sent");
                                    } else {
                                        //ToastNotify("Emergency message could not sent");
                                    }
                                }
                            }, new IntentFilter("SMS_Sent"));

                            // Store to DB push time
                            new Thread() {
                                public void run(){
                                    try {
                                        Statement statement = connection.createStatement();
                                        statement.executeUpdate("INSERT INTO userinfo (imei,phone,latitude,longitude) VALUES (" + imei + ","
                                                + number.replace("+","") + "," + lastLocation.getLatitude() + "," + lastLocation.getLongitude() + ");");
                                    } catch (SQLException e) {
                                        Log.e("MainActivity", "Insertion of push time into DB is failed: " + e);
                                    }
                                }
                            }.start();
                            // Send notifications for helpers
                            new Thread() {
                                public void run() {
                                    try {
                                        // Based on reference documentation...
                                        // http://msdn.microsoft.com/library/azure/dn223273.aspx
                                        ParseConnectionString(NotificationSettings.HubFullAccess);
                                        URL url = new URL(HubEndpoint + NotificationSettings.HubName +
                                                "/messages/?api-version=2015-01");

                                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                                        try {
                                            // POST request
                                            urlConnection.setDoOutput(true);

                                            // Authenticate the POST request with the SaS token
                                            urlConnection.setRequestProperty("Authorization",
                                                    generateSasToken(url.toString()));

                                            // Notification format should be GCM
                                            urlConnection.setRequestProperty("ServiceBusNotification-Format", "gcm");

                                            // Include any tags
                                            // Example below targets 3 specific tags
                                            // Refer to : https://azure.microsoft.com/en-us/documentation/articles/notification-hubs-routing-tag-expressions/
                                            // urlConnection.setRequestProperty("ServiceBusNotification-Tags",
                                            //        "tag1 || tag2 || tag3");

                                            // Send notification message
                                            urlConnection.setFixedLengthStreamingMode(json.length());
                                            OutputStream bodyStream = new BufferedOutputStream(urlConnection.getOutputStream());
                                            bodyStream.write(json.getBytes());
                                            bodyStream.close();

                                            // Get response
                                            urlConnection.connect();
                                            int responseCode = urlConnection.getResponseCode();
                                            if ((responseCode != 200) && (responseCode != 201)) {
                                                BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getErrorStream())));
                                                String line;
                                                StringBuilder builder = new StringBuilder("Send Notification returned " +
                                                        responseCode + " : ");
                                                while ((line = br.readLine()) != null) {
                                                    builder.append(line);
                                                }

                                                ToastNotify(builder.toString());
                                            }
                                        } finally {
                                            urlConnection.disconnect();
                                            ToastNotify("119에게 문자가 전송되고 Helper에게 알림이 보내졌습니다.");
                                        }
                                    } catch (Exception e) {
                                        Log.e("MainActivity", "Exception Sending Notification : " + e.getMessage());
                                    }
                                }
                            }.start();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (googleApiClient == null) {
            checkForLocationReuqestSetting(locationRequest);
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Log.e("debug", "api connected...");

            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            ((TextView) (findViewById(R.id.myLocation))).setText("Loading...");

            if (lastLocation != null) {
                Log.e("debug", "get last location");
                ((TextView) (findViewById(R.id.myLocation))).setText(getAddress(lastLocation));
            }

            Log.e("debug", String.valueOf(locationRequest.getInterval()));

        } catch (SecurityException ex) {
            ex.printStackTrace();
        }

        new GetAEDLocations().execute(url);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("debug", "onConnectionSuspended called...");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("debug", "onConnectionFailed called.");
    }

    @Override
    public void onLocationChanged(final Location location) {
        if (location != null) {
            Log.e("debug", "location changed");
            ((TextView) (findViewById(R.id.myLocation))).setText(getAddress(location));
            lastLocation = location;
        } else {
            ((TextView) (findViewById(R.id.myLocation))).setText("No location found..!");
        }
        //    Toast.makeText(MainActivity.this, "called...", Toast.LENGTH_SHORT).show();
    }

    public void requestPermission(int perCode, Context _c, Activity _a) {

        String fineLocationPermissionString = android.Manifest.permission.ACCESS_FINE_LOCATION;

        if (ContextCompat.checkSelfPermission(_a, fineLocationPermissionString) != PackageManager.PERMISSION_GRANTED) {
            //user has already cancelled the permission prompt. we need to advise him.
            if (ActivityCompat.shouldShowRequestPermissionRationale(_a, fineLocationPermissionString)) {
                Toast.makeText(_c, "We must need your permission in order to access your reporting location.", Toast.LENGTH_LONG).show();
            }

            ActivityCompat.requestPermissions(_a, new String[]{fineLocationPermissionString}, perCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_REQUEST_CODE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission loaded...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void checkForLocationReuqestSetting(LocationRequest locationRequest) {
        try {
            final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                    final Status status = locationSettingsResult.getStatus();
                    final LocationSettingsStates locationSettingsStates = locationSettingsResult.getLocationSettingsStates();

                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location requests here.
                            Log.d("MainActivity", "onResult: SUCCESS");
                            locationEnable = true;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.d("MainActivity", "onResult: RESOLUTION_REQUIRED");
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        MainActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                                locationEnable = false;
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.d("MainActivity", "onResult: SETTINGS_CHANGE_UNAVAILABLE");
                            // Location settings are not satisfied. However, we have no way
                            // to fix the settings so we won't show the dialog.
                            locationEnable = false;
                            break;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getAddress(Location location) {
        Log.e("debug", "address converting");

        Double latitude = location.getLatitude();
        Double longitude = location.getLongitude();

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> list = null;
        String address;

        try {
            list = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (list == null) {
            Log.e("debug", "fail to get address");
            ((TextView) (findViewById(R.id.myLocation))).setText("Fail to get address. Please try again.");
            return null;
        }

        Address addr = list.get(0);
        address = addr.getAddressLine(0);
        Log.e("debug", address);

        return address;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported by Google Play Services.");
                ToastNotify("This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void registerWithNotificationHubs() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * Example code from http://msdn.microsoft.com/library/azure/dn495627.aspx
     * to parse the connection string so a SaS authentication token can be
     * constructed.
     *
     * @param connectionString This must be the DefaultFullSharedAccess connection
     *                         string for this example.
     */
    private void ParseConnectionString(String connectionString) {
        String[] parts = connectionString.split(";");
        if (parts.length != 3)
            throw new RuntimeException("Error parsing connection string: "
                    + connectionString);

        for (int i = 0; i < parts.length; i++) {
            if (parts[i].startsWith("Endpoint")) {
                this.HubEndpoint = "https" + parts[i].substring(11);
            } else if (parts[i].startsWith("SharedAccessKeyName")) {
                this.HubSasKeyName = parts[i].substring(20);
            } else if (parts[i].startsWith("SharedAccessKey")) {
                this.HubSasKeyValue = parts[i].substring(16);
            }
        }
    }

    /**
     * Example code from http://msdn.microsoft.com/library/azure/dn495627.aspx to
     * construct a SaS token from the access key to authenticate a request.
     *
     * @param uri The unencoded resource URI string for this operation. The resource
     *            URI is the full URI of the Service Bus resource to which access is
     *            claimed. For example,
     *            "http://<namespace>.servicebus.windows.net/<hubName>"
     */
    private String generateSasToken(String uri) {

        String targetUri;
        String token = null;
        try {
            targetUri = URLEncoder
                    .encode(uri.toString().toLowerCase(), "UTF-8")
                    .toLowerCase();

            long expiresOnDate = System.currentTimeMillis();
            int expiresInMins = 60; // 1 hour
            expiresOnDate += expiresInMins * 60 * 1000;
            long expires = expiresOnDate / 1000;
            String toSign = targetUri + "\n" + expires;

            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = HubSasKeyValue.getBytes("UTF-8");
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA256");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(toSign.getBytes("UTF-8"));

            // Using android.util.Base64 for Android Studio instead of
            // Apache commons codec
            String signature = URLEncoder.encode(
                    Base64.encodeToString(rawHmac, Base64.NO_WRAP).toString(), "UTF-8");

            // Construct authorization string
            token = "SharedAccessSignature sr=" + targetUri + "&sig="
                    + signature + "&se=" + expires + "&skn=" + HubSasKeyName;
        } catch (Exception e) {
            Log.e("MainActivity", "Exception Generating SaS : " + e.getMessage());
        }

        return token;
    }

    public void ToastNotify(final String notificationMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, notificationMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void collectLocations() {
        ArrayList<Double> myLocation;

        Intent intent = new Intent(MainActivity.this, MapsActivity.class);

        try {
            myLocation = new ArrayList<>();
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

            myLocation.add(LocationServices.FusedLocationApi.getLastLocation(googleApiClient).getLatitude());
            myLocation.add(LocationServices.FusedLocationApi.getLastLocation(googleApiClient).getLongitude());
            intent.putExtra("My Location", myLocation);

            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            intent.putExtra("org", org);
            intent.putExtra("managerTel", managerTel);
            intent.putExtra("address1", address1);
            intent.putExtra("address2", address2);

            startActivity(intent);
        } catch (SecurityException ex) {
            ex.printStackTrace();
        }
    }

    private class GetAEDLocations extends AsyncTask<String, Void, Void> {
        protected Void doInBackground(String... urls) {
            latitude = new ArrayList<>();
            longitude = new ArrayList<>();
            org = new ArrayList<>();
            managerTel = new ArrayList<>();
            address1 = new ArrayList<>();
            address2 = new ArrayList<>();
            try {
                // Create and execute a SELECT SQL statement.
                // TODO: 지아 change selectSql string that it will retrieve AEDs near user's location
                Double mylat = lastLocation.getLatitude();
                Double mylong = lastLocation.getLongitude();

                String selectSql ="SELECT * FROM aedinfo WHERE acos(sin(radians(" + mylat+ ")) * sin(radians(latitude)) + cos(radians("+ mylat + ")) * cos(radians(latitude)) * cos(radians(longitude) - (radians(" + mylong + "))))* 6371 <= 3;";
                ResultSet resultSet = connection.createStatement().executeQuery(selectSql);

                while (resultSet.next()) {
                    latitude.add(Double.parseDouble(resultSet.getString(2)));
                    longitude.add(Double.parseDouble(resultSet.getString(3)));
                    org.add(resultSet.getString(5));
                    managerTel.add(resultSet.getString(6));
                    address1.add(resultSet.getString(9) + " " + resultSet.getString(10) );
                    address2.add(resultSet.getString(7) + " " + resultSet.getString(8) );
                }
            } catch (Exception e) {
                Log.e("MainActivity", e.toString());
            }

            return null;
        }
    }

    private class ConnectToDB extends AsyncTask<String, Void, Void> {
        protected Void doInBackground(String... urls) {
            try {
                Log.e("debug", "try to connect to DB");
                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                connection = DriverManager.getConnection(urls[0]);
            } catch (Exception e) {
                Log.e("MainActivity", "Failed to connect to database: " + e);
            }

            return null;
        }
    }
}
