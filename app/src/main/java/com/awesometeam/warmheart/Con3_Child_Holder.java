package com.awesometeam.warmheart;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by JIA on 2017-06-22.
 */

public class Con3_Child_Holder extends ChildViewHolder{
    private TextView subTextView;

    public Con3_Child_Holder(View itemView) {
        super(itemView);
        subTextView = (TextView)itemView.findViewById(R.id.child_subtitle);
    }

    public void setSubTextView(String name) {
        subTextView.setText(name);
    }

}
